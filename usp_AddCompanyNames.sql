--insert 
--into dbo.symbols s
--values([all], [Allstate Corporation The])
--where s.symbol = [all]

--adding column
--alter table dbo.symbols
--add [CompanyName] varchar(99)

--dropping column
--alter table dbo.symbols
--drop column [CompanyName] --varchar(99)


create proc usp_AddCompanyNames 
@companyName varchar(50),  @symbol varchar(5)
as
begin
update dbo.symbols 
set CompanyName = @companyName 
where @symbol=dbo.symbols.symbol

end


select distinct * from dbo.symbols