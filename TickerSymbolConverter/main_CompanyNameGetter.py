import csv
import numpy as np
import pyodbc
import time
from os import listdir
from os.path import isfile, join
import sys
from pprint import pprint
import re


start_time = time.time()
reference_path = "C:\Users\Administrator\Desktop\\"
file_name = r"companylist_total.csv"

data_dir = r"C:\Project\DataFromTony\dataanalyticsdata\DataAnalytics Data\allstocks_20160104"

csv_name_list = [f for f in listdir(data_dir) if (isfile(join(data_dir, f)) and f.endswith(".csv"))]

symbol_list = [smbl.split('_')[1][:-4] for smbl in csv_name_list]

symbol_comp_dict = {}
for idx, elem in enumerate(symbol_list):
    symbol_comp_dict[elem] = " "

match_count = 0

with open(reference_path+file_name, 'r') as g:

    for row, line in enumerate(g):
        split_line = line.split('","')

        rx = re.compile("[a-zA-Z]+", re.UNICODE)
        smbl_from_csv = [a.lower() for a in rx.findall(split_line[0])][0].replace("\"", '')
        company_name = [a for a in rx.findall(split_line[1])]#.replace("\"", '')
        company_name_final = ''
        for b in company_name:
            company_name_final += ' ' +b


        if (company_name_final[0]=="\"" and company_name_final[-1] == "\""):
            company_name_final = company_name_final[1:-1]
        elif (company_name_final[0]=="\""):
            company_name_final = company_name_final[1:]
        elif (company_name[-1]=="\""):
            company_name_final = company_name_final[:-1]


        for elem in symbol_list:
            if (smbl_from_csv == elem and symbol_comp_dict[elem] == " "):
                symbol_comp_dict[elem] = company_name_final
                match_count+=1


for i in symbol_comp_dict:
    value = symbol_comp_dict[i]

    if value==' ':

        symbol_comp_dict[i] = ' ' +i

with open( reference_path + 'mycsvfile.csv', 'wb') as p:

    fieldnames = ['Symbol', 'CompanyName']
    writer = csv.DictWriter(p, fieldnames=fieldnames)

    #writer.writeheader()
    for elem in symbol_comp_dict:
        company_name = symbol_comp_dict[elem]
        writer.writerow({'Symbol': elem, 'CompanyName': company_name})





