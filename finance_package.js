// Rolling Mean Algorithm
// Input: Single array of prices, Window size
// Output: Single array of length is that of input array corresponding
// to the rolling mean
function rollMean(data,win){
  var rolledData = [];
  var counter = 0;

  for(var i = 0;i < data.length;i++){
    if(i < win-1){
      rolledData.push(data[i]);
    }else{
      rolledData.push(math.mean(data.slice(counter,i+1)));
      counter += 1;
    }
  }
  return rolledData;
}

// Calculating individual daily E[R]
// Input: Single array of prices
// Return: Single value of daily expected return
function eRet(data){
  var expectRet = [];

  for(var i = 1;i<data.length;i++){
    var ret = ((data[i]-data[i-1])/data[i-1]) - 1;
    expectRet.push(ret);
  }

  return math.mean(ret);
}

// Calculating Volatility
// Input: Single array of prices
// Return: Single value corresponding to volatility
function vol(data){
  return math.std(data)
}

// Calculating porfolio return
// Input: Array of daily expected returns, Array of weights
// in portfolio of stock with corresponding expected return
// Return: Single value of daily expected return of portfolio
function eRetPort(ret,weights){
  return math.sum(math.dotMultiply(ret,weights))};

// Calculating Beta
// Input: Single array of prices, Single array of market prices
// Output: Single value representing beta
function beta(data,market){
  cov = math.mean(math.multiply(data,market)) - (math.multiply(math.mean(data),math.mean(market)))
  return cov/math.var(market)
}

