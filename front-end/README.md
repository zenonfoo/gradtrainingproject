# Citi Equity Data Analysis - Citi graduate training project designed for interactive analytics allowing users to analyse historical intra day trading activity on Stocks and ETF securities traded on NASDAQ, NYSE and AMEX exchanges. 


The Data being analysed will be from the period Jan 4th 2016 thru to 24th March 2016, containing minute by minute trading data for just over 600 stocks and ETFs.


## Built With

* [React.js](https://reactjs.org/) - A JavaScript library for building user interfaces
* [Node.js](https://nodejs.org/en/) - An asynchronous event driven JavaScript runtime used to build scalable network applications
* [HTML5/CSS3](https://html5up.net) - structuring and presenting HTML content

## Authors

* *Inderpal Panesar* - Database
* *Zeno Foo* - Server Side/Deployment
* *Serena Chan* - Server Side
* *Nijat Bakhshaliyev* - Front-End Development 


## Acknowledgments

* We would like to thank the Neueda training team for their continious assitance and guidance through this 5-day pproject.