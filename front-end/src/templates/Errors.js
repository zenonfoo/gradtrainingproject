import React from 'react';

export const Error = () => (
    <section id="wrapper">
        <header>
            <div className="Error">
                <h2>404 - Page Not Found</h2>
            </div>
        </header>
    </section>
);