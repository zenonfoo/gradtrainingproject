const math = require('mathjs')

// Rolling Mean Algorithm
// data: Single array of prices
// win: Window size
// Output: Single array of length is that of input array corresponding
// to the rolling mean
// function rollMean(data,win){
//   var rolledData = [];
//   var counter = 0;

//   for(var i = 0; i<data.length;i++){
//     if(i < win-1){
//       rolledData.push(data[i]);
//     }else{
//       rolledData.push(math.mean(data.slice(counter,i+1)));
//       counter += 1;
//     }
//   }
//   return rolledData;
// }

// Calculating individual daily E[R]
// data: Single array of prices
// Return: Single value of daily expected return
function eRet(data){
  var expectRet = [];
  for(var i = 1;i<data.length;i++){
    var ret = ((data[i]-data[i-1])/data[i-1]) - 1;
    expectRet.push(ret);
  }
  return math.mean(ret);
}

// Calculating Volatility
// data: Single array of prices
// Return: Single value corresponding to volatility
function vol(data){
  return math.std(data)
}

// Calculating Covariance
// data: Single array of prices
// market: Single array of market prices
// Output: Single value representing covariance
function cov(data,market){
  return math.mean(math.multiply(data,market)) - (math.multiply(math.mean(data),math.mean(market)));
}

// Calculating Beta
// data: Single array of prices
// market: Single array of market prices
// Output: Single value representing beta
function beta(data,market){
  return cov(data,market)/math.var(market)
}

// Calculating Portfolio Value Change
// boughtStockPrice: Single array of price when stocks were bought
// currentStockPrice: Single array of current price of stocks bought
function portValChange(boughtStockPrice,currentStockPrice){
  var price_bought = math.sum(boughtStockPrice)
  var current_price = math.sum(currentStockPrice)
  return (price_bought - current_price);
}

// Calculating Portfolio Percentage Change
// boughtStockPrice: Single array of price when stocks were bought
// currentStockPrice: Single array of current price of stocks bought
function portPercChange(boughtStockPrice,currentStockPrice){
  var price_bought = math.sum(boughtStockPrice)
  return portValChange(boughtStockPrice,currentStockPrice)/price_bought;
}

// Calculating weights in portfolio
// currentStockPrice: Single array of current price of stocks bought
function weights(currentStockPrice){
  var portfolio_weights = []
  var current_price = math.sum(currentStockPrice)
  currentStockPrice.forEach(function(inner_data){
    portfolio_weights.push(inner_data/current_price)
  })
  return portfolio_weights;
}

// Calculating daily expected porfolio return
// ret: Array of daily expected returns
// weights: Array of weights
// in portfolio of stock with corresponding expected return
// Return: Single value of daily expected return of portfolio
function eRetPort(ret,weights){
  return math.sum(math.dotMultiply(ret,weights))
}

// Calculating daily volatility
// ret: Array of array of prices of stocks in portfolio
// weights: Array of weights
// in portfolio of stock with corresponding expected return
// Return: Single value of daily expected return of portfolio
function eVolPort(arr_price,weights){
  var port_vol = [];
  for(var x = 0; x<arr_price.length-1; x++){
    for(var y = 1; y<arr_price.length; y++){
      port_vol.push(weights[x]*weights[y]*cov(arr_price[x],arr_price[y]));
    }
  }
  return math.sum(port_vol)
}
