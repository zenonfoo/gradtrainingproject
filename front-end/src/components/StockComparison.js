


import React, { Component }  from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import Highcharts from 'highcharts/highstock'
import HighchartsReact from 'highcharts-react-official'
import {Button, ButtonToolbar} from 'react-bootstrap'
import Autocomplete from './Autocomplete'


import AppleJsonObj from '../dummy_data/hourlyData.json'
import RyderJsonObj from '../dummy_data/hourlyData_r.json';



async function getJSONData() {
    
  let dataPoints = AppleJsonObj.map( (entry) => {
      let date_string = entry.idd.toString();
      let format = date_string.slice(0,4) + "-" + date_string.slice(4,6) + "-" + date_string.slice(6,8) +"T"+date_string.slice(10,12) +":00:00"
      let date_t = new Date(format)
      let y = [date_t.getTime(), entry.open, entry.high, entry.low, entry.close ]
      return y 
    }); 

    let collectivePoints = 
      {
            name: 'AAPL Stock Price',
            data: dataPoints 
      }

    return collectivePoints;
}


const ListStocks = ({ items, onItemClick }) => (
  <ul>
    {
      items.map((item, i) =>   <button key={i} value={item} className="button button3" onClick={() => onItemClick(item)}>{item}</button>
)
    }
  </ul>
);

//

export class StockComparison extends Component {

    constructor(props) {
        super(props);

        this.state = {
          error: null,
          errorInfo: null,

          stocksDisplayed : [],


          yAxis: {
              labels: {
                  formatter: function () {
                      return (this.value > 0 ? ' + ' : '') + this.value + '%';
                  }
              },
              plotLines: [{
                  value: 0,
                  width: 2,
                  color: 'silver'
              }]
          },

          plotOptions: {
              series: {
                  compare: 'percent',
                  showInNavigator: true
              }
          },

          tooltip: {
              pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
              valueDecimals: 2,
              split: true
          },

          series: []

        };

    }

    componentDidMount() {
      
      getJSONData().then( result => {

      this.setState({
          series: this.state.series.concat(result)
        });

      });

    }

    removeStock = (val) =>{

      let updated_data = this.state.series;
      let stocksDisplayed = this.state.stocksDisplayed;
      
      let index = stocksDisplayed.indexOf(val);

      for(let i = 0; i<updated_data.length; i++)
      { if(updated_data[i].name == val)
          {
            updated_data.splice(i, 1);  
          }
      }

      stocksDisplayed.splice(index, 1);

        this.setState({
          stocksDisplayed: stocksDisplayed,
          data: updated_data
        });
    }    

    handleClick = (stockName) => {
      
      if(this.state.stocksDisplayed.indexOf(stockName) == -1)
      {
        if (this.state.stocksDisplayed.length<=4){
          this.setState({ 
            stocksDisplayed: this.state.stocksDisplayed.concat([stockName]) 
          });
          this.populateData(stockName);
        }
        else {alert('To many stocks to compare, please remove'); console.log(this.state.stocksDisplayed)}
      }
    }

    populateData = (stockName) => {

        let jsonData = RyderJsonObj.map( (entry) => {
              let date_string = entry.idd.toString();
              let format = date_string.slice(0,4) + "-" + date_string.slice(4,6) + "-" + date_string.slice(6,8) +"T"+date_string.slice(10,12) +":00:00"
              let date_t = new Date(format)
              let y = [date_t.getTime(), entry.open, entry.high, entry.low, entry.close ]
              return y 
            }); 

        let result = {
          name: stockName,
          data: jsonData
        }

        this.setState({
          series: this.state.series.concat(result)
        
        });

    }

    updateBase = (stockName) => {
    	console.log("pidor")
    	let updated_data = this.state.series
    	
        let jsonData = RyderJsonObj.map( (entry) => {
          let date_string = entry.idd.toString();
          let format = date_string.slice(0,4) + "-" + date_string.slice(4,6) + "-" + date_string.slice(6,8) +"T"+date_string.slice(10,12) +":00:00"
          let date_t = new Date(format)
          let y = [date_t.getTime(), entry.open, entry.high, entry.low, entry.close ]
          return y 
        }); 

        let result = {
          name: stockName,
          data: jsonData
        }

    	updated_data[0] = result

    	this.setState({
          series: updated_data
        
        });


    }
    

     componentDidCatch(error, errorInfo) {
        // Catch errors in any components below and re-render with error message
        this.setState({
          error: error,
          errorInfo: errorInfo
        })
      // You can also log error messages to an error reporting service here
      }
    
    render() {
      if (this.state.errorInfo) {
        // Error path
        return (
          <div>
            <h2>Something went wrong.</h2>
            <details style={{ whiteSpace: 'pre-wrap' }}>
              {this.state.error && this.state.error.toString()}
              <br />
              {this.state.errorInfo.componentStack}
            </details>
          </div>
	        );
	    }

	    return (
	    	<div>
	    	        
		    	<h3> Please choose the base stock for comparison </h3>
		    	 <Autocomplete handleClick={this.updateBase.bind(this)} />
		    	 &nbsp;
		    	  <HighchartsReact
		    	    highcharts={Highcharts}
		    	    constructorType={'stockChart'}
		    	    options={this.state} />
				&nbsp;
		        <h3>
		        	Please search for a stock in the menu below and it will get updated on the graph  
		        	<i className="fa fa-search fa-lg" aria-hidden="true"  /> 
		        	</h3> 

		        <Autocomplete handleClick={this.handleClick.bind(this)} />
		         <div>
		          &nbsp;
		          {this.state.stocksDisplayed.length > 0 && <h3> Displayed Stocks (Click to remove):</h3>}
		          <ListStocks items={this.state.stocksDisplayed} onItemClick={this.removeStock} />
		        </div>
	        </div>
	      );
    }
}

