import React, { Component }  from 'react';
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import 'mathjs';


var CanvasJSReact = require ('../utils/canvasjs.react');
var CanvasJSChart = CanvasJSReact.CanvasJSChart;
var userTransactionData = require ('../dummy_data/UserTransactionData.json');
var financeFunctions = require('../utils/finance_package.js')

export class Portfolio extends Component{
	constructor(props) {
		super(props);
		
		let total = 0;
		userTransactionData.Transactions.forEach(function(el) {
			total += el.quantityPurchased * el.priceAtPurchase
		})
		this.state = {
			utList : userTransactionData.Transactions.map((d) => { 
				var amountSpent = d.quantityPurchased * d.priceAtPurchase;
				var y = Math.round(amountSpent/total * 100);
				return { "y" : y , "label" : d.companyName, "amountSpent" : amountSpent.toFixed(2)}
			}),
			total: total
		}
		
	}
	

	render() {
		const options = {
			exportEnabled: true,
			animationEnabled: true,
			title: {
				text: "User 1 Portfolio"
			},
			data: [{
				type: "pie",
				startAngle: 75,
				toolTipContent: "<b>{label}</b>: ${amountSpent}",
				showInLegend: "true",
				legendText: "{label}",
				indexLabelFontSize: 14,
				indexLabel: "{label} - {y}%",
				dataPoints: this.state.utList
			}]
		}
		return (
		<div>
			<div class="col-md-8">
				<CanvasJSChart options = {options}
					/* onRef={ref => this.chart = ref} */
				/>
				{/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
			</div>
			<div class="col-md-4">
				<table class="table">
					<thead>
						<tr>
						<th scope="col">#</th>
						<th scope="col">First</th>
						<th scope="col">Last</th>
						<th scope="col">Handle</th>
						</tr>
					</thead>
					<tbody>
						<tr>
						<th scope="row">Portfolio Value</th>
						<td>Mark</td>
						<td>Otto</td>
						<td>@mdo</td>
						</tr>
						<tr>
						<th scope="row">Daily Expected Return</th>
						<td>Jacob</td>
						<td>Thornton</td>
						<td>@fat</td>
						</tr>
						<tr>
						<th scope="row">Volatility</th>
						<td>Larry</td>
						<td>the Bird</td>
						<td>@twitter</td>
						</tr>
						<tr>
						<th scope="row">Beta</th>
						<td>Larry</td>
						<td>the Bird</td>
						<td>@twitter</td>
						</tr>
						
					</tbody>
					</table>
			</div>
		</div>
		);
	}
}