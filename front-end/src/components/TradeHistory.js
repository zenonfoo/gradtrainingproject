import React, {Component} from 'react'
import Autocomplete from './Autocomplete'
import {Button, ButtonToolbar, DropdownButton, MenuItem} from 'react-bootstrap'

import jsonObj from '../dummy_data/hourlyData.json'


var CanvasJSReact = require ('../utils/canvasjs.react');
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


const ListStockItem = ({ value, onClick }) => (
  <button value={value} className="button button3" onClick={() => onClick(value)}>{value}</button>
);

//


const ListStocks = ({ items, onItemClick }) => (
  <ul>
    {
      items.map((item, i) => <ListStockItem key={i} value={item} onClick={onItemClick} />)
    }
  </ul>
);

//

const  ListTimeSelect = ({ name, items, onItemClick }) => (
  <DropdownButton bsSize="xsmall" bsStyle='default' title={name} id="dropdown-size-extra-small">
  { 
    items.map( (item, i) => <MenuItem key={i} onClick={() => onItemClick(item)}> {item}</MenuItem> )
  }
  </DropdownButton>
);

//
export class TradeHistory extends Component {
 
     constructor(props) {
        super(props);

        this.handleClick = this.handleClick.bind(this);
        this.removeStock = this.removeStock.bind(this);
        this.setMonth = this.setMonth.bind(this);
        this.setDay = this.setDay.bind(this);
        this.setHour = this.setHour.bind(this);

        this.state = {
            stocksDisplayed:[],

            months:['January','February', 'March'],
            selectedMonth: 'Month',
            selectedDay: 'Day',
            setMonthNum: 0,
            selectedHour: 'Hour',
            numDays:[],
            numHours:['Hour','9','10','11','12','13','14','15'],
            
            

            theme: "light2", // "light1", "light2", "dark1", "dark2"
        animationEnabled: true,
        exportEnabled: true,
        
        title:{
          text: "Intel Corporation Stock Price -  2016"},
        
        axisX: {
          valueFormatString: "MMM"},
        
        axisY: {
          includeZero:false,
          prefix: "$",
          title: "Price (in USD)" },
        
        data: [{
          type: "candlestick",
          showInLegend: true,
          name: "Intel Corporation",
          yValueFormatString: "$###0.00",
          xValueFormatString: "MMMM YY",
          dataPoints: []
        }]
      };
    }

    onInputUpdate = (stockName) =>{

      this.setState({ 
        data: this.state.data.concat([
        {
        type: "candlestick",
        showInLegend: true,
        name: stockName,
        yValueFormatString: "$###0.00",
        xValueFormatString: "MMMM YY",
        dataPoints: [
          { x: new Date("2016-01-01"), y: [38.61, 39.45, 34.19, 39.82] },
          { x: new Date("2016-02-01"), y: [38.82, 38.95, 32.84, 39.20] },
          { x: new Date("2016-03-01"), y: [38.85, 39.30, 36.66, 39.07] },
          { x: new Date("2016-04-01"), y: [38.19, 39.50, 38.21, 39.15] },
          { x: new Date("2016-05-01"), y: [38.11, 34.17, 32.02, 39.11] },
          { x: new Date("2016-06-01"), y: [38.12, 39.57, 37.34, 39.74] },
          { x: new Date("2016-07-01"), y: [37.51, 33.86, 37.23, 39.47] },
          { x: new Date("2016-08-01"), y: [37.66, 34.70, 37.38, 39.07] },
          { x: new Date("2016-09-01"), y: [37.24, 35.15, 37.93, 38.08] },
          { x: new Date("2016-10-01"), y: [39.12, 48.80, 30.08, 49.49] },
          { x: new Date("2016-11-01"), y: [47.97, 41.30, 45.77, 49.84] },
          { x: new Date("2016-12-01"), y: [41.73, 41.64, 46.67, 49.16] }
        ]
      }
          ])
    })

    }

    handleClick = (stockName) => {
      
      if(this.state.stocksDisplayed.indexOf(stockName) == -1)
      {
        this.setState({ 
          stocksDisplayed: this.state.stocksDisplayed.concat([stockName]) 
        })

        this.onInputUpdate(stockName);
      }
  }

  removeStock = (val) =>{

    let updated_data = this.state.data;
    let stocksDisplayed = this.state.stocksDisplayed;
    
    let index = stocksDisplayed.indexOf(val);

    for(let i = 0; i<updated_data.length; i++)
    { if(updated_data[i].name == val)
        {
          updated_data.splice(i, 1);  
        }
    }

    stocksDisplayed.splice(index, 1);

      this.setState({
        stocksDisplayed: stocksDisplayed,
        data: updated_data
      });
  }

  setMonth = (selected) => {

    let setMonthNum = null;
    let days=[];

    days[0]='Day'

    if (selected =='February'){ 
      setMonthNum = 1;
      for(let i=1; i<=28; i++)
        days.push(i);
    }

    else {
      
      for(let i=1; i<=31;i++) days.push(i);

      if (selected =='January') setMonthNum = 0;
      else if(selected=='March') setMonthNum = 2;
    }

    var dataPoints = [];
    jsonObj.map( (entry) =>
    {
      let date_string = entry.idd.toString();
      let format = date_string.slice(0,4) + "-" + date_string.slice(4,6) + "-" + date_string.slice(6,8) +"T"+date_string.slice(10,12) +":00:00"
      
      let date_t = new Date(format)
      let y = [entry.open, entry.high, entry.low, entry.close ]

      if(date_t.getMonth()==setMonthNum){
        let pos = dataPoints.map(function(e) { return e.x.getDate(); }).indexOf(date_t.getDate());

          if (pos >=0 ){
            let temp = dataPoints[pos];
            if (temp.y[4]==null) temp.y[4]=1;
            for (let i=0;i<4;i++)
              temp.y[i] += y[i];
            
            temp.y[4]+=1;
            dataPoints[pos] = temp;
          }
          else dataPoints.push({
          'x': date_t, 
          'y': y
        })
      }   
    });


    let correctData = [];
    dataPoints.map( (entry) => { 
      
      let y =[]
      for (let i=0;i<=3;i++){
        y.push(entry.y[i]/entry.y[4])
      }

      correctData.push({
        'x': entry.x, 
        'y': y
      })
    })


    this.setState({
      selectedDay: 'Day',
      numDays: days,
      selectedMonth: selected,
      setMonthNum: setMonthNum,

      axisX: {valueFormatString: "MMMM DD",
          title: "Date" },
      
      axisY: {
        includeZero:false,
        prefix: "$",
        title: "Price (in USD)" },

      data: [
          {
          type: "candlestick",
          showInLegend: true,
          name: jsonObj[0].companyName,
          yValueFormatString: "$###0.00",
          xValueFormatString: "MMMM DD",
          dataPoints: correctData     
        }
        ]
    });

  }

  setDay = (selected) => {

    var dataPoints = [];
    var json_data = jsonObj.map( (entry) =>
    {
      
      let date_string = entry.idd.toString();
      let format = date_string.slice(0,4) + "-" + date_string.slice(4,6) + "-" + date_string.slice(6,8) +"T"+date_string.slice(10,12) +":00:00"
      
      let date_t = new Date(format)
      let y = [entry.open, entry.high, entry.low, entry.close ]
          
      if(date_t.getMonth()==this.state.setMonthNum && date_t.getDate()==selected){
        dataPoints.push({
          'x': date_t, 
          'y': y
        })
      }
    });

    this.setState({
      selectedDay: selected,

      axisX: {valueFormatString: "HH",
          title: "Hours" },
      
      axisY: {
        includeZero:false,
        prefix: "$",
        title: "Price (in USD)" },

      data: [
          {
          type: "candlestick",
          showInLegend: true,
          name: jsonObj[0].companyName,
          yValueFormatString: "$###0.00",
          xValueFormatString: "DDDD HH",
          dataPoints: dataPoints
        
        }
        ]
    });
  }

  setHour = (selected) => {
    this.setState({
      selectedHour: selected
    });


  }

  render() {

    return (
    <div>

      <h3> Your Watch List </h3>

      {/*
      <button> Line Chart</button> 
      <button> Candle Stick</button> 
      <button> Volume </button> 
      <button> Moving Average 1</button>  
      <button> Moving Average 2</button>
      */}

      <CanvasJSChart options = {this.state}  onRef={ref => this.chart = ref} />

      &nbsp;
      <h3>Select timeframes (Month, Day, Hour):</h3> 
        <ListTimeSelect name={this.state.selectedMonth} items={this.state.months} onItemClick={this.setMonth} />
        &nbsp;
        {this.state.selectedMonth != 'Month' && <ListTimeSelect name={this.state.selectedDay} items={this.state.numDays} onItemClick={this.setDay} />}
        &nbsp;
        {this.state.selectedDay != 'Day' && <ListTimeSelect name={this.state.selectedHour} items={this.state.numHours} onItemClick={this.setHour} />}

        {/*You can get reference to the chart instance as shown above using onRef. This allows you to access all chart properties and methods*/}
      
      <Autocomplete handleClick={this.handleClick.bind(this)} />
       <div>
        &nbsp;
        {this.state.stocksDisplayed.length > 0 && <h3>Displayed Stocks (Click to remove):</h3>}
        <ListStocks items={this.state.stocksDisplayed} onItemClick={this.removeStock} />
      </div>
        
    </div>
    );
  }
}


