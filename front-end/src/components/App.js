import React, { Component } from 'react';
import Routes from './Routes'
import {Link, withRouter} from 'react-router-dom'

// comment code
// Login.js - implement login for given username & clean up and comment the code
// StockComparison.js -  implement api fetching & clean up and comment the code
// Stockanalysis.js - implement api fetching, 
//
// (Serena)
// Portfolio.js - Implement or delete table, add WatchList => if possible to send POST request and update back-end?? => add graphs comparison (from single )
// ADD TRY & CATCH 

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: true
    };
  }


  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated });
  }
  
  handleLogout = async event => {
    //await Auth.signOut();
    this.userHasAuthenticated(false);
    this.props.history.push("/login");

  }

 componentDidCatch(error, errorInfo) {
        // Catch errors in any components below and re-render with error message
        this.setState({
          error: error,
          errorInfo: errorInfo
        })
      // You can also log error messages to an error reporting service here
      }
    
    render() {

       const childProps = {
          isAuthenticated: this.state.isAuthenticated,
          userHasAuthenticated: this.userHasAuthenticated
        };
        if (this.state.errorInfo) {
            // Error path
            return (
              <div>
                <h2>Something went wrong.</h2>
                <details style={{ whiteSpace: 'pre-wrap' }}>
                  {this.state.error && this.state.error.toString()}
                  <br />
                  {this.state.errorInfo.componentStack}
                </details>
              </div>
            );
          }


        return (
        <div>
       
          <header id="header">
            <h1>
              <Link to="/">CitiCMDA</Link>
            </h1>

            <nav className="main">
              <ul>
                {/* <li className="search">
                  <a className="fa-search" href="#search">Search</a>
                  <form id="search" method="get" action="#">
                    <input type="text" name="query" placeholder="Search" />
                  </form>
                </li> */}
                <li>
                  <a className="fa-user  " href="#menu">Menu</a>
                </li>

              </ul>
            </nav>
          </header>

          {/* Menu */}
          <nav id="menu">
            <div className="inner">
              <h2>Menu</h2>

              <section>
                <ul className="links">
                  <li>
                    <Link to="/analytics">
                      <h3>Single Stock Analytics</h3>
                    </Link>
                  </li>
                  <li>
                    <Link to="/comparison">
                      <h3>Multiple Stocks Comparison</h3>
                    </Link>
                  </li>
                  <li>
                    <Link to="/portfolio">
                      <h3>Portfolio</h3>
                    </Link>
                  </li>
                </ul>
              </section>

              {/*Fix THIS */}
              {this.state.isAuthenticated && 
                  <section>
                  <ul className="actions vertical"> 
                     <button onClick={this.handleLogout}> Logout </button> 

                  </ul>
                  </section>
                }
              
              

                </div>
              </nav>

              <Routes childProps={childProps} />
             </div>
        );
  }
}

export default withRouter(App);

