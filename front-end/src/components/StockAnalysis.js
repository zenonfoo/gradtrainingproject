import React, { Component }  from 'react';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import Highcharts from 'highcharts/highstock';
import HighchartsReact from 'highcharts-react-official';

import symbolJSON from '../dummy_data/AllSymbolData.json'

//import update from 'immutability-helper';
import Autocomplete from './Autocomplete'


import AppleJsonObj from '../dummy_data/hourlyData.json';
import RyderJsonObj from '../dummy_data/hourlyData_r.json';


async function getJSONData(id) {
    
	let volumePoints = [],
	 dataPoints = AppleJsonObj.map( (entry) => {
      let date_string = entry.idd.toString();
      let format = date_string.slice(0,4) + "-" + date_string.slice(4,6) + "-" + date_string.slice(6,8) +"T"+date_string.slice(10,12) +":00:00"
      let date_t = new Date(format)
      let y = [date_t.getTime(), entry.open, entry.high, entry.low, entry.close ]
      let v = [date_t.getTime(), entry.volume]
      volumePoints.push(v);
      return y;
    }); 
    

    let collectivePoints = 
    [
    	{
		    type: id,
   	        name: 'AAPL Stock Price',
            data: dataPoints 
    	},
    	{
    		type: 'column',
            name: 'Volume',
            yAxis: 1,
            data: volumePoints,
    	}
    ] 

    return collectivePoints;
}

export class StockAnalysis extends Component {

    constructor(props) {
        super(props);


        this.state = {
          error: null,
          errorInfo: null,

          displayStocks : ['AAPL','Ryder'],
          stockName: 'Apple',

          title: {
              text: 'Stock Price'
          },

          yAxis: [{
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: ' Stock Price'
            },
            height: '60%',
            lineWidth: 2,
            resize: {
                enabled: true
            }
          }, {
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Volume'
            },
            top: '65%',
            height: '35%',
            offset: 0,
            lineWidth: 2
          }],

	        tooltip: {
	            split: true
	        },

	        rangeSelector: {
				buttons: [{
					type: 'day',
					count: 1,
					text: 'd'
				},{
					type: 'week',
					count: 1,
					text: 'w'
				},{
					type: 'month',
					count: 1,
					text: 'm'
				}, {
					type: 'all',
					text: 'All'
				}]
			},

          series:[]
        };

    }

    componentDidMount() {
      
      getJSONData('candlestick').then( result => {

      this.setState({ 
          series: result
        });
      	
      	if(result.name!="Volume"){
      		result.data
      	}

      });
    }

     componentDidCatch(error, info) {
        // Display fallback UI
        this.setState({ hasError: true });
        // You can also log the error to an error reporting service
        console.log(error, info);
      }



     updateBase = (stockName) => {
      console.log("pidor")
      let updated_data = this.state.series
      
        let jsonData = RyderJsonObj.map( (entry) => {
          let date_string = entry.idd.toString();
          let format = date_string.slice(0,4) + "-" + date_string.slice(4,6) + "-" + date_string.slice(6,8) +"T"+date_string.slice(10,12) +":00:00"
          let date_t = new Date(format)
          let y = [date_t.getTime(), entry.open, entry.high, entry.low, entry.close ]
          return y 
        }); 

        let result = {
          name: stockName,
          data: jsonData
        }

      updated_data[0] = result

      this.setState({
          series: updated_data
        
        });


    }

  componentDidCatch(error, errorInfo) {
        // Catch errors in any components below and re-render with error message
        this.setState({
          error: error,
          errorInfo: errorInfo
        })
      // You can also log error messages to an error reporting service here
      }
    
    render() {
      if (this.state.errorInfo) {
        // Error path
        return (
          <div>
            <h2>Something went wrong.</h2>
            <details style={{ whiteSpace: 'pre-wrap' }}>
              {this.state.error && this.state.error.toString()}
              <br />
              {this.state.errorInfo.componentStack}
            </details>
          </div>
        );
      }

      return(
      	<div>
          <h3> Please choose the base stock for comparison </h3>
           <Autocomplete handleClick={this.updateBase.bind(this)} />
           &nbsp;
  	    	  <HighchartsReact
  	    	    highcharts={Highcharts}
  	    	    constructorType={'stockChart'}
  	    	    options={this.state}
  	    	  />

        		{/*<ReactTable data={data} columns={columns} />*/}
          </div>
        );

      }
}