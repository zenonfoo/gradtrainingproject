import React from "react";
import { Route, Switch } from "react-router-dom";


import {Portfolio} from './Portfolio'
import {StockAnalysis} from './StockAnalysis'
import {StockComparison} from './StockComparison'
import {Login} from './Login'
import {Error} from '../templates/Errors'

import AuthenticatedRoute from "./AuthenticatedRoute";
import UnauthenticatedRoute from "./UnauthenticatedRoute";

export default ({ childProps }) =>
  <Switch>
    <AuthenticatedRoute path="/" exact component={Portfolio} props={childProps} />
    <UnauthenticatedRoute path="/login" exact component={Login} props={childProps} />
    <AuthenticatedRoute path="/portfolio" exact component={Portfolio} props={childProps} />
    <AuthenticatedRoute path="/analytics" exact component={StockAnalysis} props={childProps} />
    <AuthenticatedRoute path="/comparison" exact component={StockComparison} props={childProps} />

    { /* Finally, catch all unmatched routes */ }
    <Route component={Error} />
  </Switch>;