import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import {Link} from 'react-router'


export class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      password: ""
    };
  }

  validateForm() {
    return this.state.id.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

// handleSubmit = async event => {
//   event.preventDefault();

//   try {
//     await Auth.signIn(this.state.email, this.state.password);
//     this.props.userHasAuthenticated(true);
//     this.props.history.push("/");
//   } catch (e) {
//     alert(e.message);
//   }
// }

  handleSubmit = event => {
    event.preventDefault();
    this.props.userHasAuthenticated(true);
    this.props.history.push("/");

  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="id" bsSize="large">
            <ControlLabel>Login ID</ControlLabel>
            <FormControl
              autoFocus
              value={this.state.id}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          
          <Button
            block
            bsSize="small"
            disabled={!this.validateForm()}
            type="submit"
          >
            Login
          </Button>
        </form>
      </div>
    );
  }
}