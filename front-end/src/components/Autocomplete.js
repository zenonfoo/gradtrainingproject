import React, {Component} from "react";
import jsonData from "../dummy_data/AllSymbolData.json";

class Autocomplete extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      suggestions : jsonData.map((d) => { 
        var accName = `[${d.symbol}] ${d.companyName}`;
        return accName
      }),

      activeTab: 0,
      matchingSuggestions: [],
      displayList: false,
      userInput: ''
    };

  }

  // Event triggered for any input value change
  onChange = e => {

    const suggestions  = this.state.suggestions;
    const userInput = e.currentTarget.value;

    // Filter suggestion list to match client input
    const matchingSuggestions = suggestions.filter(
      suggestion => suggestion.toLowerCase().indexOf(userInput.toLowerCase()) > -1
    );

    // Update the user input and matching suggestions
    this.setState({
      activeTab: 0,
      matchingSuggestions,
      displayList: true,
      userInput: e.currentTarget.value
    });
  };

  // Event triggered for an input click
  onClick = e => {

    // Update the user input and reset the rest of the state
    this.setState({
      activeTab: 0,
      matchingSuggestions: [],
      displayList: false,
      userInput: ''
    });

    // Pass the selected item to the parent class
    this.props.handleClick(e.currentTarget.innerText);

  };

  // Event riggered for any key down
  onKeyDown = e => {
    const { activeTab, matchingSuggestions } = this.state;

    // User pressed the enter key,
    if (e.keyCode === 13) {
      this.setState({
        activeTab: 0,
        displayList: false,
        userInput: ''
      });

      if(this.state.matchingSuggestions[this.state.activeTab] != null)
        this.props.handleClick(this.state.matchingSuggestions[this.state.activeTab]);

    }
    // User pressed the up arrow
    else if (e.keyCode === 38) {
      if (activeTab === 0) {
        return;
      }

      this.setState({ activeTab: activeTab - 1 });
    }
    // User pressed the down arrow
    else if (e.keyCode === 40) {
      if (activeTab - 1 === matchingSuggestions.length) {
        return;
      }

      this.setState({ activeTab: activeTab + 1 });
    }
  };

  render() {
    const {
      onChange,
      onClick,
      onKeyDown,
      state: {
        activeTab,
        matchingSuggestions,
        displayList,
        userInput
      }
    } = this;

    let suggestionsListComponent;

    if (displayList && userInput) {
      if (matchingSuggestions.length) {
        suggestionsListComponent = (
          <ul className="suggestions">
            {matchingSuggestions.map((suggestion, index) => {
              
              let className;

              if (index === activeTab) {
                className = "suggestion-active";
              }

              return (
                <li
                  className={className}
                  key={suggestion}
                  onClick={onClick}
                >
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } 

    else {
        suggestionsListComponent = (
          <div className="no-suggestions">
            <em>No suggestions, please check your spelling!</em>
          </div>
        );
      }
    }

    return (
      <div>
        &nbsp;      
        &nbsp;
        <input
          type="text"
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
        />
        {suggestionsListComponent}
        </div>
    );
  }
}

export default Autocomplete;