-- Script to initialise everything in the database
if  exists(select * from sys.databases d where d.name='ProjectData')
begin
use master;
alter database ProjectData set single_user with rollback immediate;
drop database ProjectData
end

GO
CREATE DATABASE ProjectData
ON(   
	NAME = 'ProjectData'
	, FILENAME = 'C:\Project\data\ProjectData.mdf' 
	, SIZE = 20MB 
	, FILEGROWTH = 50%
	)
	LOG ON( 
	NAME = 'ProjectData_log'
	, FILENAME = N'C:\Project\logs\ProjectData_log.ldf' 
	, SIZE = 1MB 
	, FILEGROWTH = 50%
	);
GO
use ProjectData;
GO
--if exists(select * from sys.tables t join sys.schemas s on t.schema_id=s.schema_id where s.name = 'dbo' and t.name='stagingTable')
--drop table dbo.stagingTable;
--GO
create table dbo.stagingTable
(
Date int
, Time int
, [Open] smallmoney
, High smallmoney
, Low smallmoney
, [Close] smallmoney
, Volume real
, SplitFactor real
, Earnings int
, Dividends varchar(99)
)
GO
--if exists(select * from sys.tables t join sys.schemas s on t.schema_id=s.schema_id where s.name = 'dbo' and t.name='AllFileData')
--drop table dbo.AllFileData;

create table dbo.AllFileData
(
ID int identity(1,1)  CONSTRAINT PK_AllFileData primary key
, symbolID int
, symbol varchar(5)
, Date int
, Time int
, [Open] smallmoney
, High smallmoney
, Low smallmoney
, [Close] smallmoney
, Volume  real -- number of stocks --
, SplitFactor real
, Earnings int
, Dividends float
)
GO
create table symbols(symbolID int identity(1,1) constraint symbolsPK primary key, symbol varchar(5))
GO

 create Table Calendar(
 CalendarDate date primary key
 ,intDate int -- join with Date in HourlyAggregates
 ,dayNumber int
 ,dayName varchar(30)
 ,shortDayName varchar(10)
 ,monthNumber int
 ,monthName varchar(30)
 ,shortMonthName varchar(10)
 ,yearNumber int
 ,shortStringDate varchar(30) -- Mon 17 Feb
 ,longStringDate varchar(50) -- Monday 17th February 
 );
 GO

 --create Table DailyAggregate(
 --ID int CONSTRAINT PK_DailyAggregate primary key 

 --)
 --drop table DailyAggregate

 -- 60 rows from allfilestable condensed into 1 row for hourly

 create Table HourlyAggregate(
 ID int /* not identity(1,1)*/  CONSTRAINT PK_Aggs primary key
, symbolID int
--, symbol varchar(99)
, Date int 
, Time int -- time of each hour, 950 is 10
, [Open] smallmoney -- opening price of that hour
, High smallmoney
, Low smallmoney
, [Close] smallmoney
, Volume  real
, SplitFactor real
, Earnings int
, Dividends float
);
GO

--if exists(select * from sys.procedures p join sys.schemas s on p.schema_id=s.schema_id where s.name = 'dbo' and p.name='ImportOneCSV')
--drop procedure ImportOneCSV;
--GO
create procedure usp_ImportOneCSV
@fullFilename varchar(999), @symbol varchar(5)
AS
BEGIN
declare @sql varchar(999)
	--1 import into stage 1 ------------------------------
	--triple quotes surround a variable to be added into the string
	--char(10) = carriage return
	set @sql = 'BULK INSERT dbo.stagingTable FROM ''' 
					+ @fullFilename 
					+ ''' WITH (FIELDTERMINATOR = '','', ROWTERMINATOR ='''+CHAR(10)+''' ,TABLOCK)';
	exec(@sql);
	--2 copy to stage 2, adding symbol -----------------
	insert into AllFileData
	--setting all symbolIDs and syombols as 0 initially, to be changed in csharp/java code
	select 0 as symbolID, @symbol as symbol 
	,[Date] 
, [Time] 
, [Open] 
, High 
, Low 
, [Close] 
, Volume
, SplitFactor 
, Earnings 
, Dividends 
--stage1 deleted after use, like a cache
 from stagingTable;

	--3 delete from stage 1 ------------------------------
	truncate table stagingTable
END;
GO


create procedure usp_populateSymbolTable
AS
Begin
	insert into symbols
	--symbols=table
		select distinct symbol from AllFileData;
		--so we definitely get every symbol id, as not all are traded everyday
		 
	update AllFileData
	set symbolID = s.symbolID
	--iserting the symbol column from symbols table
	from AllFileData d join symbols s on d.symbol = s.symbol;
End
Go

create procedure usp_FillDatesTable
as
begin
	WITH Dates as
	 (
	 SELECT convert(Date,'2015-01-01')  as CalendarDate
	 UNION ALL
	 SELECT dateadd(day , 1, CalendarDate) AS CalendarDate
	 FROM Dates
	 WHERE dateadd (day, 1, CalendarDate) < '2020-01-01'
	 )
	 Insert Into Calendar
	 select	CalendarDate
			,Convert(int,Replace(Convert(varchar,CalendarDate),'-',''))
			,DatePart(day,CalendarDate)
			,DateName(weekday,CalendarDate)
			,Left(DateName(weekday,CalendarDate),3)
			,DatePart(month,CalendarDate)
			,DateName(month,CalendarDate)
			,Left(DateName(month,CalendarDate),3)
			,DatePart(year,CalendarDate)
			,Convert(varchar(99),CalendarDate,106) -- 30 Dec 2006
			,Convert(varchar(99),CalendarDate,103) -- Dec 30, 2006
	from Dates
	 OPTION (MAXRECURSION 32767);
end;
GO

create procedure usp_PopulateAggsTable 
as
begin
	With CTE as
	(
	SELECT min(ID) AS OpeningID
				,max(ID) AS ClosingID
				,symbolID
				--, symbol -- not needed here anymore. 
				,DATE
				,TIME / 100 AS 'theHour'
				--, [Open] smallmoney -- get this later
				,max(High) AS maxHi
				,min(Low) AS minLow
				--, [Close]  -- get this later
				,Sum(Volume) AS 'sumVolume'
				, avg(SplitFactor) AS 'avgSplitFactor'
				,Sum(Earnings) AS 'sumEarnings'
				,Sum(Dividends) AS 'sumDividends'
	FROM AllFileData d
	GROUP BY symbolID  ,DATE , TIME / 100 -- 24 rows long for each day
	) 
	 Insert into HourlyAggregate(
	 
								ID , symbolID , 
								Date,  Time, [Open],  
								High, Low , [Close] , 
								Volume  , SplitFactor , 
								Earnings , Dividends )
	-- get open and close here 
	select OpeningID, CTE.symbolID, CTE.[Date], CTE.theHour, 
	d1.[Open], maxHi, minLow, d2.[close]
	,sumVolume, avgSplitFactor,  sumEarnings, sumDividends
	from CTE 
	join AllFileData d1 on d1.id = CTE.OpeningID 
	join AllFileData d2 on d2.ID = CTE.ClosingID
end;
GO

--NB: After uploading the data, execute thes three stored procedures: (just highlight without the -- and execute)
--usp_populateSymbolTable  --might take a minute
--usp_FillDatesTable				 -- 2 seconds!
usp_PopulateAggsTable     --might take a minute

--
--Now... Create indexes on all search fields (eg contents of where clauses) and foreign keys eg CREATE INDEX i1 ON t1 (col1);

/*sql express is free but uses only 1GB RAM, and one core of CPU.
--sql server developer" is also free and is same as full SQL Server but to beused only for development!

--These indexes should be created AFTER the data has been uploaded!!!  <------- LOOK HERE :-)

Create index idx_AllFileData_symbolID on AllFileData(symbolID);
Create index idx_AllFileData_Date on  AllFileData([Date]);
--
Create index idx_HourlyAggregate_symbolID on HourlyAggregate(symbolID);
Create index idx_HourlyAggregate_Date on  HourlyAggregate([Date]);
Create index idx_CalDayName on Calendar(dayname);
Create index idx_SymbolsByName on  Symbols(symbol);
--
Create index idx_Calendar_MonthNumber on Calendar(monthNumber)
--
--
*/

/* Here is a comparison of two queries, one calling the original table, and the other calling the optimised tables:


select symbol,date,sum(volume) from AllFileData
where symbol like 'G%'  and datename(weekday,convert(date,(convert(varchar(99),[date]))))='friday'
group by symbol,date
order  by symbol,date

select symbol,date,sum(volume) from HourlyAggregate a join symbols s on a.symbolID = s.symbolID
join Calendar c on c.intDate = a.Date
where s.symbol like 'G%' and c.dayName='friday'
group by symbol,date
order by symbol,date

*/