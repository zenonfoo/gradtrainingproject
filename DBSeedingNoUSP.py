import csv
import numpy as np
import pyodbc
import time
from os import listdir
from os.path import isfile, join
import sys
from pprint import pprint
start_time = time.time()
path = "C:\Project\DataFromTony\dataanalyticsdata\DataAnalytics Data\\"

# https://stackoverflow.com/questions/35308946/connect-sql-server-to-python-3-with-pyodbc/47947018#47947018
# https://stackoverflow.com/questions/37422532/pyodbc-connection-to-mssql
# cnxn is sql for us
# import pandas as pd
# df = pd.read_sql_query('select * from table', cnxn)


# con = sql.connect(r"DRIVER={SQL Server Native Client 11.0}", host=server, database=db1,user =user, password=pword)
# con = sql.connect(
#     'DRIVER={SQL Server Native Client 11.0};'
#     'SERVER=WIN-70OAN17IPPO\SQLEXPRESS,1434;'
#     'trusted_connection=yes')

# 'DATABASE=ProjectData;UID=Fred;PWD=Fred')

def getConnection():

    server = "WIN-70OAN17IPPO\SQLEXPRESS"
    port = 1434
    db1 = "ProjectData"
    user="Fred"
    pword = "Fred"

    con = pyodbc.connect(Driver='{SQL Server}',
                      Server=server + (',%s' %port),
                      Database=db1,
                      Trusted_Connection='Yes')
    return con



cnxn = getConnection()
cursor = cnxn.cursor()
count = 0
for day_folder in listdir(path):
    nested_folder_directory = path + day_folder

    csv_list = [f for f in listdir(nested_folder_directory) if (isfile(join(nested_folder_directory, f)) and f.endswith(".csv"))]

    for csv in csv_list:

        fullFileName = nested_folder_directory + '\\' + csv
        symbol = csv.split('_')[1][:-4]

        # usp_import_one_csv = "usp_ImportOneCSV @fullFilename='" + fullFileName \
        #                      + "', @symbol='" + symbol + "'"
        # print(usp_import_one_csv)
        #
        # cursor.execute(usp_import_one_csv)
        # cursor.commit()

        # query = """ INSERT INTO dbo.AllFileData
        #             SELECT
        #
        #             1,'a',20160104,930,41.08,41.08,41.04,41.06,990,1,0,0
        #
        #  """

        # with open(fullFileName, 'r') as file:

        query = """BULK INSERT INTO 
                    dbo.AllFileData
                    FROM
                    %s
                    WITH (FIELDTERMINATOR = '','',
                            ROWTERMINATOR = '''
        
        
        """ %fullFileName

        cursor.execute(query)
        cursor.commit()

        # "
        # insert
        # into
        # dbo.stagingTable
        # select
        #
        # ID=int, symbolID=int, symbol=str ,20160104, 930, 41.08, 41.08, 41.04, 41.06, 990, 1, 0, 0
        # "

        if count == 3:
            sys.exit()

        count+=1
        if (count % 1000 == 0):
            print("CSV: %s, Symbol: %s -- Time Elapsed: %0.4s"
                      %(csv, symbol, (time.time() - start_time)))

cursor.close()
cnxn.close()
# querystring = r"select * from Orders"
#
# query_result = cursor.execute(querystring)

# for row in query_result.fetchall():
#     print row[3]


