--AllFileData indices
Create index IX_AllFileData_symbolID
on dbo.AllFileData(symbolID);

Create index IX_AllFileData_symbol
on dbo.AllFileData(symbol);

Create unique index IX_AllFileData_Date
on dbo.AllFileData([Date]);
--date in AllFileData is intDate in Calendar table

--Symbols Table indices
Create index IX_Symbols_Symbol
on dbo.Symbols([Symbol]);

--Calendar Table Indices
create unique index IX_CalendarDate_intDate
on dbo.Calendar(intDate);








--Create index IX_TableName_ColumnToBeIndexed
--on dbo.TableName(ColumnToBeIndexed)


--Drop index IX_TableName_ColumnToBeIndexed
--on TableName