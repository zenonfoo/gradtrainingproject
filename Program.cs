using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static String urlConnection = @"server=.\:1500;Database=ProjectData;integrated security=true;User id=Fred;Password=Fred";
       // static String urlConnection = @"server=.\;database=ProjectData;integrated security=true";
        // //WIN-70OAN17IPPO\\SQLEXPRESS:1500;databaseName=ProjectData;user=Fred;password=Fred
        static void Main(string[] args)
        {
            System
            importOneCSV();
        }

        public static void importOneCSV()
        {
            var startTime = DateTime.Now;
            var conn = new SqlConnection(urlConnection);
            var usp = new SqlCommand("usp_ImportOneCSV",conn);
            usp.CommandType = CommandType.StoredProcedure;
            usp.Parameters.Add("@fullFilename", SqlDbType.VarChar);
            usp.Parameters.Add("@symbol", SqlDbType.VarChar);

            int i = 0;
            var symbol = "";

            string[] files = Directory.GetFiles(@"C:\ProjectData", "*.csv", SearchOption.AllDirectories);
            try
            {
                conn.Open();
                foreach (var file in files)
                {
                    symbol = Path.GetFileNameWithoutExtension(file).Substring(6);
                    usp.Parameters["@fullFileName"].Value = file;
                    usp.Parameters["@symbol"].Value = symbol;
                    Console.WriteLine(file);
                    Console.WriteLine(symbol);
                    usp.ExecuteNonQuery();
                    if (i % 500 == 0) System.Diagnostics.Debug.WriteLine($"Completed lines {i}");
                    i++;
                }
            } catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
            conn.Close();
            var endTime = DateTime.Now;
            Console.WriteLine($"{i} files copied: {(endTime - startTime).TotalSeconds} seconds");

        }
    }
}
