package com.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.AllFileData.Table;
import com.AllFileData.TableFunc;

@RestController
@RequestMapping("/api")
public class StockAnalysisController {
	
	@Autowired
    TableFunc table;
	
	// define endpoint for "/employee"
	@GetMapping("/stocksymbol/{symbolID}")
	public List<Table> getStock(@PathVariable int symbolID){	
		return table.getStockBySymbol(symbolID);
	}
}
