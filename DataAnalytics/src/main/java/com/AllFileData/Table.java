package com.AllFileData;

import java.util.List;

public class Table {
	
	private int ID;
	private String Ticker;
	private String Name;
	// ask about date
	// ask about time
	private Float Open;
	private Float Close;
	private Float High;
	private Float Low;
	// will this be an int or a float?
	private Float Volume;
	private Float SplitFactor;
	private Float Earnings;
	private Float Dividends;
	
	public int getID() {
		return ID;
	}
	public void setID(int iD) {
		ID = iD;
	}
	public String getTicker() {
		return Ticker;
	}
	public void setTicker(String ticker) {
		Ticker = ticker;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public Float getOpen() {
		return Open;
	}
	public void setOpen(Float open) {
		Open = open;
	}
	public Float getClose() {
		return Close;
	}
	public void setClose(Float close) {
		Close = close;
	}
	public Float getHigh() {
		return High;
	}
	public void setHigh(Float high) {
		High = high;
	}
	public Float getLow() {
		return Low;
	}
	public void setLow(Float low) {
		Low = low;
	}
	public Float getVolume() {
		return Volume;
	}
	public void setVolume(Float volume) {
		Volume = volume;
	}
	public Float getSplitFactor() {
		return SplitFactor;
	}
	public void setSplitFactor(Float splitFactor) {
		SplitFactor = splitFactor;
	}
	public Float getEarnings() {
		return Earnings;
	}
	public void setEarnings(Float earnings) {
		Earnings = earnings;
	}
	public Float getDividends() {
		return Dividends;
	}
	public void setDividends(Float dividends) {
		Dividends = dividends;
	}
	
	

	
}
