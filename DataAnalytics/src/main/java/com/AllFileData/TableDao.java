package com.AllFileData;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
public class TableDao extends JdbcDaoSupport{
	
	@Autowired 
	DataSource dataSource;
	
	@PostConstruct
	private void initialize(){
		setDataSource(dataSource);
	}
	
	public List<Table> getStockBySymbol(int symbol) {
		// SQL Statement To Be Written
		String sql = "SQL Statement To Be Written";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);
		
		List<Table> result = new ArrayList<Table>();
		for(Map<String, Object> row:rows){
			Table tab = new Table();
			//tab.set.. to be populated here
			result.add(tab);
		}
		
		return result;
	}
	

}
