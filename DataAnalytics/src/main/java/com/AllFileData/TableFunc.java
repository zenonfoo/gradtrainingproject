package com.AllFileData;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TableFunc{
	
	@Autowired
	TableDao tableDao;
	
	public List<Table> getStockBySymbol(int symbol){
		List<Table> tab = tableDao.getStockBySymbol(symbol);
		return tab;
	}
}
